
singleton Material(Floor_Material)
{
   mapTo = "floor_mat";
   diffuseMap[0] = "art/pbr/floor/FloorEbony_basecolor.png";
   normalMap[0] = "art/pbr/floor/FloorEbony_normal.png";
   roughMap[0] = "art/pbr/floor/FloorEbony_rough.png";
   aoMap[0] = "art/pbr/floor/FloorEbony_ao.png";
   metalMap[0] = "art/pbr/floor/FloorEbony_metal.png";
   invertSmoothness[0] = "1";
   useAnisotropic[0] = "1";
   translucentBlendOp = "None";
   materialTag0 = "pbr";
};
