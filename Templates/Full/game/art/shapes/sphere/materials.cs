singleton Material(Sphere_material)
{
   mapTo = "Sphere_mat";
   diffuseColor[0] = "1 0 0 1";
   smoothness[0] = "1";
   metalness[0] = "1";
   useAnisotropic[0] = "1";
   materialTag0 = "Miscellaneous";
};

